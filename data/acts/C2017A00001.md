---
title: Freedom of Speech Act 2017
introducer: dunce_confederacy
assent: 2017-04-20
urls:
- "https://www.reddit.com/r/AustraliaSim/comments/60p2ok/b2_first_reading_freedom_of_speech_act_2017/"
- "https://www.reddit.com/r/AustraliaSim/comments/66homs/royal_assent_of_approved_legislation/"
amends:
- Racial Discrimination Act 1975
---

A Bill for an Act to amend the Racial Discrimination Act 1975, and for related purposes.

**PART 1 INTRODUCTION**

Section 1 Short Title

This Act may be cited as the Freedom of Speech Act 2017.

Section 2 Commencement

This Act commences on the day after this Act receives the Royal Assent.

Section 3 Objectives

The objective of this bill is to

* (a) increase the freedom of speech enjoyed by Australians.

**PART 2 AMENDMENTS**

Section 4 Racial Discrimination Act 1975 Section 18C

* (a) Repeal the section.

Section 5 Racial Discrimination Act 1975 Section 18D

* (a) Repeal the section.
