---
title: Crimes Legislation Amendment (Age of Criminal Responsibility) Act 2019
introducer: elliellia
assent: 2019-12-06
urls:
- "https://drive.google.com/file/d/1wS7oXGJxIRj4ecW9FMN58isIf97G49sR/view?usp=sharing"
- "https://www.reddit.com/r/AustraliaSim/comments/e6xe88/gazette_20_of_2019_assent_to_bills_no_4_2019/"
amends:
- Crimes Act 1914
---