---
title: Code of Conduct Act 2017
introducer: General_Rommel
number: 14
year: 2017
assent: 2017-12-07
urls:
- https://www.reddit.com/r/AustraliaSim/comments/7i1fcr/presentation_of_medical_services_dying_with/
repeals:
- C2017A00003
---
