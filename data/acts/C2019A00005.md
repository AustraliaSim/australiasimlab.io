---
title: Legislation Clean-up (Omnibus) Amendment Act 2019
introducer: PineappleCrusher_
assent: 2019-02-08
urls:
- https://docs.google.com/document/d/1Z0SEC7nM99aMDorOjgn6ZO9AuylzpItNNeXA9lGfG_w/edit
amends:
- Racial Discrimination Act 1975
- Marriage Act 1961
- C2017A00011
- C2018A00005
- C2018A00015
- C2018A00019
- C2017A00018
repeals:
- C2017A00001
- C2017A00004
- C2018A00021
- C2017A00014
- C2018A00004
---
