---
title: Australian Broadcasting Corporation Amendment (Appointment of Directors) Act 2019
introducer: Dyljam
assent: 2019-06-02
urls:
- https://docs.google.com/document/d/1mDjoUVB7FKCpHTjtBrL5SLewtyQnvdoxslKJPXcSKHI/edit
- https://docs.google.com/document/d/18IXaiVlZciAGR9SLjz7KN7OJNctuCyTT1-soaUP7c1o/edit
amends:
- Australian Broadcasting Corporation Act 1983
---
