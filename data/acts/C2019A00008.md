---
title: Competition and Consumer (Free Range Eggs) Amendment Act 2019
introducer: dyljam
assent: 2019-03-02
urls:
- https://docs.google.com/document/d/1p5f0oaSzU-tAq-jrEsfvs8NYtNuvYkw8fULrdl_Ynv0/edit
- https://docs.google.com/document/d/1fvTQLxPgxrYcLWlknyCYCT8ze7-x3I71pAJ6k3DX8P0/edit
amends:
- Competition and Consumer Act 2010
---
