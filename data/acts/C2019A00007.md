---
title: Parliamentary Participation (Overhaul) Act 2019
introducer: lieselta
assent: 2019-03-02
urls:
- https://docs.google.com/document/d/1fvTQLxPgxrYcLWlknyCYCT8ze7-x3I71pAJ6k3DX8P0/edit
amends:
- C2019A00005
---
