import frontmatter
import hashlib
from jinja2 import Markup
from markdown import markdown
import os
import subprocess

class Document:
	def __init__(self, code=None, metadata=None, content=None):
		self.code = code
		self.metadata = metadata
		self.content = content
		
		self.autoconverted = {}
	
	@property
	def number(self):
		if 'number' in self.metadata:
			return self.metadata['number']
		return int(self.code[-5:])
	
	@property
	def year(self):
		if 'year' in self.metadata:
			return self.metadata['year']
		return int(self.code[1:5])
	
	@property
	def formats(self):
		return [filename.split('.')[1] for filename in sorted(os.listdir(os.path.join('data', self.FOLDER_NAME))) if filename.startswith(self.code)]
	
	@property
	def can_render(self):
		if self.content:
			return True
		if 'html' in self.autoconverted:
			return True
		if 'md' in self.autoconverted:
			return True
		if 'txt' in self.autoconverted:
			return True
		return False
	
	def render(self):
		if self.content:
			return Markup(markdown(self.content))
		if 'html' in self.autoconverted:
			with open(self.autoconverted['html'], 'r') as f:
				return Markup(f.read())
		if 'md' in self.autoconverted:
			with open(self.autoconverted['md'], 'r') as f:
				return Markup(markdown(f.read()))
		if 'txt' in self.autoconverted:
			with open(self.autoconverted['txt'], 'r') as f:
				return Markup('<pre style="max-height: 80vh; overflow: scroll;">' + f.read() + '</pre>')
		return None
	
	def autoconvert(self):
		formats = self.formats
		if 'docx' in formats:
			args = ['pandoc', os.path.join('data', self.FOLDER_NAME, self.code + '.docx'), '-t', 'html5', '--self-contained']
			out_format = 'html'
		elif 'odt' in formats:
			args = ['pandoc', os.path.join('data', self.FOLDER_NAME, self.code + '.odt'), '-t', 'html5', '--self-contained']
			out_format = 'html'
		elif 'pdf' in formats:
			args = ['pdftotext', '-layout', os.path.join('data', self.FOLDER_NAME, self.code + '.pdf'), '-']
			out_format = 'txt'
		else:
			return None
		
		m = hashlib.sha256()
		m.update(' '.join(args).encode('utf-8'))
		out_file = os.path.join('cache', self.code + '.' + m.hexdigest() + '.' + out_format)
		os.makedirs(os.path.dirname(out_file), exist_ok=True)
		
		if os.path.exists(out_file):
			print('Using cached {} for {}'.format(out_file, ' '.join(args)))
		else:
			print('Running {}'.format(' '.join(args)))
			result = subprocess.run(args, capture_output=True, encoding='utf-8')
			with open(out_file, 'w') as f:
				print(result.stdout, file=f)
		
		self.autoconverted[out_format] = out_file
		
		return out_format
	
	@classmethod
	def from_file(cls, code):
		data = frontmatter.load(os.path.join('data', cls.FOLDER_NAME, code + '.md'))
		return cls(code, data.metadata, data.content.strip())

class Act(Document):
	FOLDER_NAME = 'acts'
	
	def __init__(self, code=None, metadata=None, content=None):
		super().__init__(code, metadata, content)
		
		self.amended_by = []
		self.repealed_by = []
		self.compilations = []
		self.instruments = []

class Compilation(Document):
	FOLDER_NAME = 'compilations'
	
	def __init__(self, code=None, metadata=None, content=None):
		super().__init__(code, metadata, content)
		
		self.principal = None
		self.upto = None
	
	@classmethod
	def from_file(cls, code, acts_bycode):
		compilation = super().from_file(code)
		compilation.principal = acts_bycode[compilation.metadata['principal']]
		compilation.upto = acts_bycode[compilation.metadata['upto']]
		return compilation

class Instrument(Document):
	FOLDER_NAME = 'instruments'
	
	def __init__(self, code=None, metadata=None, content=None):
		super().__init__(code, metadata, content)
		
		self.made_under = None
		self.amended_by = []
		self.repealed_by = []
	
	@classmethod
	def from_file(cls, code, acts_bycode):
		instrument = super().from_file(code)
		if instrument.metadata['made_under'] in acts_bycode:
			instrument.made_under = acts_bycode[instrument.metadata['made_under']]
		return instrument

def generate(core):
	# Read Acts
	
	acts = []
	for filename in sorted(os.listdir(os.path.join('data', 'acts'))):
		if filename.endswith('.md'):
			act = Act.from_file(filename[:-3])
			acts.append(act)
			
			# Convert to text
			if not act.content:
				act.autoconvert()
	
	acts_bycode = {act.code: act for act in acts}
	
	for act in acts:
		if 'amends' in act.metadata:
			for reference in act.metadata['amends']:
				if len(reference.split()) == 1:
					acts_bycode[reference].amended_by.append(act)
		if 'repeals' in act.metadata:
			for reference in act.metadata['repeals']:
				if len(reference.split()) == 1:
					acts_bycode[reference].repealed_by.append(act)
	
	# Read Compilations
	
	compilations = []
	for filename in sorted(os.listdir(os.path.join('data', 'compilations'))):
		if filename.endswith('.md'):
			compilation = Compilation.from_file(filename[:-3], acts_bycode)
			compilations.append(compilation)
			acts_bycode[compilation.code] = compilation
			
			# Convert to text
			if not compilation.content:
				compilation.autoconvert()
	
	for compilation in compilations:
		compilation.principal.compilations.append(compilation)
	
	# Read Instruments
	
	instruments = []
	for filename in sorted(os.listdir(os.path.join('data', 'instruments'))):
		if filename.endswith('.md'):
			instrument = Instrument.from_file(filename[:-3], acts_bycode)
			instruments.append(instrument)
			acts_bycode[instrument.code] = instrument
			
			# Convert to text
			if not instrument.content:
				instrument.autoconvert()
	
	for instrument in instruments:
		if instrument.made_under:
			instrument.made_under.instruments.append(instrument)
		if 'amends' in instrument.metadata:
			for reference in instrument.metadata['amends']:
				if len(reference.split()) == 1:
					acts_bycode[reference].amended_by.append(instrument)
		if 'repeals' in instrument.metadata:
			for reference in instrument.metadata['repeals']:
				if len(reference.split()) == 1:
					acts_bycode[reference].repealed_by.append(instrument)
	
	# Indexes
	
	core.render('legislation/', 'frlsim/index.html')
	core.render('legislation/acts/bydate/', 'frlsim/acts_bydate.html', acts=acts)
	core.render('legislation/acts/bytitle/', 'frlsim/acts_bytitle.html', acts=acts)
	core.render('legislation/acts/inforce/bytitle/', 'frlsim/acts_inforce_bytitle.html', acts=[act for act in acts if not act.repealed_by])
	core.render('legislation/acts/compilations/bytitle/', 'frlsim/acts_compilations_bytitle.html', compilations=compilations)
	core.render('legislation/instruments/bydate/', 'frlsim/instruments_bydate.html', instruments=instruments)
	core.render('legislation/instruments/bytitle/', 'frlsim/instruments_bytitle.html', instruments=instruments)
	
	# Act
	
	for act in acts:
		core.render('legislation/act/{}/'.format(act.code), 'frlsim/act_view.html', act=act, acts_bycode=acts_bycode)
		for format in act.formats:
			core.copy('legislation/act/{0}/download/{0}.{1}'.format(act.code, format), 'data/acts/{}.{}'.format(act.code, format))
		for format, out_file in act.autoconverted.items():
			core.copy('legislation/act/{0}/download/{0}.auto.{1}'.format(act.code, format), out_file)
	
	# Compilation
	
	for compilation in compilations:
		core.render('legislation/compilation/{}/'.format(compilation.code), 'frlsim/compilation_view.html', compilation=compilation)
		for format in compilation.formats:
			core.copy('legislation/compilation/{0}/download/{0}.{1}'.format(compilation.code, format), 'data/compilations/{}.{}'.format(compilation.code, format))
		for format, out_file in compilation.autoconverted.items():
			core.copy('legislation/compilation/{0}/download/{0}.auto.{1}'.format(compilation.code, format), out_file)
	
	# Instrument
	
	for instrument in instruments:
		core.render('legislation/instrument/{}/'.format(instrument.code), 'frlsim/instrument_view.html', instrument=instrument, acts_bycode=acts_bycode)
		for format in instrument.formats:
			core.copy('legislation/instrument/{0}/download/{0}.{1}'.format(instrument.code, format), 'data/instruments/{}.{}'.format(instrument.code, format))
		for format, out_file in instrument.autoconverted.items():
			core.copy('legislation/instrument/{0}/download/{0}.auto.{1}'.format(instrument.code, format), out_file)
